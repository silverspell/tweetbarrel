'''
Created on 28 Haz 2013

@author: Cem.Guler
'''


from flask import *
from contextlib import closing
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy import func
import json
from datetime import timedelta
from functools import update_wrapper

SQLALCHEMY_DATABASE_URI = "mysql://root:1234@localhost/twipdb"
DEBUG = True
SECRET_KEY = "devkey"
USERNAME = "admin"
PASSWORD = "default"

app = Flask(__name__)
app.config.from_object(__name__)

db = SQLAlchemy(app)



def crossdomain(origin="*", methods=["GET", "POST", "OPTIONS"], headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator


class Tweet(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    from_user = db.Column(db.String(255))
    body = db.Column(db.Text())
    created_at = db.Column(db.String(255))
    moderated = db.Column(db.Integer())
    profile_picture = db.Column(db.Text())
    
    def __init__(self, from_user, body, created_at, moderated, pp):
        self.body = body
        self.created_at = created_at
        self.from_user = from_user
        self.moderated = moderated
        self.profile_picture = pp

    def __repr__(self):
        return "<Tweet %r>" % self.body
    
    def to_j(self):
        return {"user": self.from_user, "img": self.profile_picture,\
                            "body": self.body, \
                         "created_at": self.created_at, "moderated": self.moderated, "id": self.id}


class Admin(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(255))
    password = db.Column(db.String(255))
    
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __repr__(self):
        return "<Admin %r>" % self.username

class TwitterSettings(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    access_token = db.Column(db.String(255))
    access_token_secret = db.Column(db.String(255))
    consumer_key = db.Column(db.String(255))
    consumer_secret = db.Column(db.String(255))

    def __init__(self, access_token, access_token_secret, consumer_key, consumer_secret):
        self.access_token = access_token
        self.access_token_secret = access_token_secret
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret

    def __repr__(self):
        return "<TwitterSettings %r>" % self.access_token

class AppSettings(db.Model):
    key = db.Column(db.String(255), primary_key = True)
    val = db.Column(db.Text())
    
    def __init__(self, key, val):
        self.key = key
        self.val = val
        
    def __repr__(self):
        return "<AppSettings %r>" % self.key

class Trackers(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    trackers = db.Column(db.Text())
    def __init__(self, trackers):
        self.trackers = trackers
        
    def __repr__(self):
        return "<Trackers %r>" % self.id

@app.after_request
def shutdown_session(response):
    db.session.remove()
    return response


@app.route("/")
def index():
    abort(404)

@app.route("/add_tweet", methods=["GET", "POST"])
def add_tweet():
    if request.method == "GET":
        return render_template("test.html")
    else:
        tbody = request.form["body"]
        tfrom = request.form["from"]
        tcreate_date = request.form["create_date"]
        tprofile_picture = request.form["profile_picture"] 
        t = Tweet(tfrom, tbody, tcreate_date, 0, tprofile_picture)
        try:
            db.session.add(t)
            db.session.commit()
        except:
            db.session.rollback()
            
        return generate_response({"status": "OK"})


@app.route("/list_tweets", methods=["GET"])
@app.route("/list_tweets/<int:moderated>", methods=["GET"])
@crossdomain()
def list_tweets(moderated=0):
    tweets = Tweet.query.filter_by(moderated = moderated).order_by(Tweet.created_at.desc()).all()
    res = [tweet.to_j() for tweet in tweets]
    return generate_response({"status": "OK", "cnt": len(res), "tweets":res})


@app.route("/moderate_tweet", methods=["POST"])
@crossdomain()
def moderate_tweet():
    tweet = Tweet.query.filter_by(id=request.form["id"]).first()
    #print tweet
    #1 approve, #2 disapprove
    tweet.moderated = request.form["moderated_value"]
    db.session.commit()
    return generate_response({"status": "OK"})

@app.route("/user/login", methods=["POST", "OPTIONS"])
@crossdomain()
def login():
    username = request.form["username"]
    password = request.form["password"]
    admin = Admin.query.filter((Admin.username == username) & (Admin.password == password)).first()
    stat = admin is not None and "OK" or "FAIL"
    obj = {"status": stat}
    return generate_response(obj)
    

@app.route("/user/changepass", methods=["POST"])
@crossdomain()
def changepass():
    password = request.form["Password"]
    admin = Admin.query.first()
    admin.password = password
    db.session.commit()
    return generate_response({"status": "OK"})

@app.route("/mode/counter", methods=["GET"])
@app.route("/mode/counter/<int:num>", methods=["GET"])
@crossdomain()
def counter(num = 5):
    tweets = db.session.query(func.count(Tweet.id), Tweet.from_user, Tweet.profile_picture) \
                            .group_by(Tweet.from_user) \
                            .order_by(func.count(Tweet.id).desc()) \
                            .filter(Tweet.moderated == 1)[:num]
    ret = [{"user": tweet[1], "count": tweet[0], "img": tweet[2]} for tweet in tweets]
    return generate_response({"tweets": ret})


@app.route("/mode/mention/", methods=["GET"])
@app.route("/mode/mention/<int:last>", methods=["GET"])
@crossdomain()
def mention(last = 0):
    tweets = Tweet.query.filter((Tweet.moderated == 1) & (Tweet.id > last))
    ret = []
    for tweet in tweets:
        ret.append(tweet.to_j())
    return generate_response({"status": "OK", "count": len(ret), "tweets": ret})
    
@app.route("/mode/show", methods=["GET"])
@crossdomain()
def show_mode():
    mode = AppSettings.query.filter(AppSettings.key == "MODE")
    return generate_response({"status": "OK", "mode": mode[0].val})


@app.route("/mode/set", methods=["POST"])
@crossdomain()
def set_mode():
    mode = request.form["mode"]
    if mode != "Counter" and mode != "Mention":
        return generate_response({"status": "FAIL"})
    m = AppSettings.query.filter(AppSettings.key == "MODE")[0]
    m.val = mode
    db.session.commit()
    return generate_response({"status": "OK"})


@app.route("/tracker/show", methods=["GET"])
@crossdomain()
def show_trackers():
    trackers = Trackers.query.all()
    ret = [tracker.trackers for tracker in trackers]
    return generate_response({"status": "OK", "trackers": ret})

@app.route("/tracker/set", methods=["POST"])
@crossdomain()
def set_tracker():
    Trackers.query.delete()
    trackers = json.loads(request.form["trackers"])
    for tracker in trackers:
        t = Trackers(tracker)
        db.session.add(t)
    db.session.commit()
    signal_restart()
    return generate_response({"status": "OK"})

@app.route("/settings/show", methods=["GET"])
@crossdomain()
def show_settings():
    settings = TwitterSettings.query.first()
    resp = generate_response({"status": "OK", "settings": {"access_token": settings.access_token, \
                                                           "access_token_secret": settings.access_token_secret, \
                                                           "consumer_key": settings.consumer_key, \
                                                           "consumer_secret": settings.consumer_secret \
                                                           }})
    return resp

@app.route("/settings/edit", methods=["POST"])
@crossdomain()
def edit_settings():
    at = request.form["access_token"] or ""
    ats = request.form["access_token_secret"] or ""
    ck = request.form["consumer_key"] or ""
    cs = request.form["consumer_secret"] or ""
    
    if at == "" or ats == "" or ck == "" or cs == "":
        return generate_response({"status": "FAIL"})
    
    TwitterSettings.query.delete()
    ts = TwitterSettings(at, ats, ck, cs)
    db.session.add(ts)
    db.session.commit()
    signal_restart()
    return generate_response({"status": "OK"})

@app.route("/truncate", methods=["GET"])
@crossdomain()
def truncate():
    db.session.execute("truncate tweet")
    return generate_response({"status": "OK"})


def generate_response(obj):
    resp = make_response(json.dumps(obj))
    resp.headers["Content-type"] = "application/json"
    return resp

def signal_restart():
    restart_required = AppSettings.query.filter(AppSettings.key == "CHANGED").first()
    restart_required.val = "1"
    db.session.commit()
    

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=1044, debug=True)

