'''
Created on 28 Haz 2013

@author: Cem.Guler
'''

import tweepy
import sys
import requests
import service
import time, threading






class StreamListener(tweepy.StreamListener):
    
    RUNNING = False 
    #CONSUMER_KEY = "ZGWWlKACvMdNXonAlPJA"
    #CONSUMER_SECRET = "95ISMpWVYGG9nCtaoKEYcjso7WYrnROIV7o5QxTRc"
    
    #ACCESS_TOKEN = "97053306-ZEzzC1fNcfmYqCFKwzuoIPxkRCSKgkk798f5Un9D8" 
    #ACCESS_TOKEN_SECRET = "xjIU9xNld19twhIWWnK4lLsnbMPWkZwMM6jhhylSDE"
    
    def set_tokens(self, ck, cs, at, ats):
        self.ACCESS_TOKEN = at
        self.ACCESS_TOKEN_SECRET = ats
        self.CONSUMER_KEY = ck
        self.CONSUMER_SECRET = cs


    def auth(self):
        auth1 = tweepy.auth.OAuthHandler(self.CONSUMER_KEY, self.CONSUMER_SECRET)
        auth1.set_access_token(self.ACCESS_TOKEN, self.ACCESS_TOKEN_SECRET)
        return auth1
        
    def on_error(self, status_code):
        print >> sys.stderr, "Error with code", status_code
        return True
    
    def on_timeout(self):
        print >> sys.stderr, "Timeout"
        return True 
    
    def on_status(self, status):
        try:
            payload = {"body": status.text, "from": status.author.screen_name, "create_date": status.created_at, "profile_picture": status.user.profile_image_url}
            r = requests.post("http://localhost:1044/add_tweet", data=payload)
            
        except Exception, e:
            print >> sys.stderr, "Error status: ", e
        

def main():
    
    restart_required = service.AppSettings.query.filter(service.AppSettings.key == "CHANGED").first()
    if restart_required.val == "1" or not StreamListener.RUNNING:
        StreamListener.RUNNING = True
        restart_required.val = "0"
        service.db.session.commit()
        ts = service.TwitterSettings.query.first()
        l = StreamListener()
        l.set_tokens(ts.consumer_key, ts.consumer_secret, ts.access_token, ts.access_token_secret)
        auth1 = l.auth()
        api = tweepy.API(auth1)
        streamer = tweepy.Stream(auth=auth1, listener=l, timeout=30000000)
        trackers = [t.trackers for t in service.Trackers.query.all()]
        streamer.filter(None, trackers, True)
    threading.Timer(20, main).start()
        
    
    
        
if __name__ == "__main__":
    main()
    
